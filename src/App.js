import Board from "./components/Board";
import { calculateWinner } from "./utils/calculate-winner";

const App = () => {
  const squares = Array(9)
    .fill(null)
    .map((_, i) => i + 1);

  return (
    <>
      <h1 style={{ textAlign: "center" }}>Tic Tac Toe</h1>
      <Board squares={squares} calculateWinner={calculateWinner} />
    </>
  );
};

export default App;
