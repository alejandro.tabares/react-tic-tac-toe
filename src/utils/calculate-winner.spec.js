import { calculateWinner } from "./calculate-winner";

describe("Calculate winner", () => {
  it.todo("has to return the player symbol when that player wins");
  it.todo("has to return `null` when no player has won yet");
  it.todo(
    "has to throw an Error when the game is over (no more squares left to click"
  );
});
